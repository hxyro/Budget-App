# Budget-App


## Docker & CI/CD

files:

[Dockerfile](https://gitlab.com/hxyro/Budget-App/-/blob/main/Dockerfile)
[CI/CD files](https://gitlab.com/hxyro/Budget-App/-/blob/main/.gitlab-ci.yml)


### Video demo
[![Video](https://cdn.discordapp.com/attachments/1011891705437503498/1012604206244438026/unknown.png)](https://gitlab.com/hxyro/Budget-App/-/blob/main/videos/1st.mp4)

[gdrive link](https://drive.google.com/file/d/1smu9yM95jqsyXqLtqDMLhcGtBkIDy99K/view?usp=sharing)

### run locally with docker compose

```
git clone https://gitlab.com/hxyro/Budget-App.git
cd Budget-App
docker compose up -d
```
this will start the dev server on [localhost:3000](http://localhost:3000)

## K8S

files:

[k8s](https://gitlab.com/hxyro/Budget-App/-/tree/main/k8s)
```
k8s
├── app.yaml
├── configmap.yaml
├── ingress.yaml
├── PersistentVolume.yaml
├── postgres.yaml
├── redis.yaml
└── secret.yaml
```
### Video demo
[![Video](https://cdn.discordapp.com/attachments/1011891705437503498/1012604206244438026/unknown.png)](https://gitlab.com/hxyro/Budget-App/-/blob/main/videos/2nd.mp4)

[gdrive link](https://drive.google.com/file/d/1CvDXqxEUUUKwPNsCb-mhjavyWVPLP-TU/view?usp=sharing)

### run locally
```
git clone https://gitlab.com/hxyro/Budget-App.git
cd Budget-App/k8s
kubectl apply -f .
kubectl port-forward svc/app-service 3000:80
```

