FROM ruby:3.1.2-alpine as builder

ENV BUNDLER_VERSION=2.3.6
ENV LANG=C.UTF-8 \
  BUNDLE_JOBS=4 \
  BUNDLE_RETRY=3


RUN apk add --update --no-cache \
  binutils-gold \
  build-base \
  curl \
  file \
  g++ \
  gcc \
  git \
  less \
  libstdc++ \
  libffi-dev \
  libc-dev \ 
  linux-headers \
  libxml2-dev \
  libxslt-dev \
  libgcrypt-dev \
  make \
  netcat-openbsd \
  nodejs \
  openssl \
  pkgconfig \
  postgresql-dev \
  python3 \
  tzdata \
  yarn 

RUN gem install bundler -v 2.3.6
WORKDIR /app

COPY Gemfile Gemfile.lock ./

RUN bundle config build.nokogiri --use-system-libraries

RUN bundle check || bundle install --jobs=10

COPY package.json /

RUN yarn install --check-files && yarn cache clean
EXPOSE 3000

COPY . ./ 

FROM ruby:3.1.2-alpine as prod
WORKDIR /app
RUN apk add \
  tzdata \
  nodejs \
  postgresql-dev
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/
COPY . /app
# ENV RAILS_ENV production
ENV NODE_ENV production
EXPOSE 3000
ENTRYPOINT ["./entrypoints/docker-entrypoint.sh"]


